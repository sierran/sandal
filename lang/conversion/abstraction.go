package conversion

import (
	"fmt"
	"strings"
)

func abstractIr1(mods []intModule) (error, []intModule) {
	//err, newMods := replaceConditionsWithTrue(mods)
	err, newMods := removeEpsFromIr1(mods)
	if err != nil {
		return err, nil
	}
	//	err, newMods = abstractForDebug(newMods)
	//	if err != nil {
	//		return err, nil
	//	}
	return nil, newMods
}

// ==== Abstraction: These abstractions are just for debugging ====

func abstractForDebug(mods []intModule) (error, []intModule) {
	retIntMods := []intModule{}
	for _, mod := range mods {
		var intMod intModule
		var err error
		switch mod := mod.(type) {
		case intMainModule:
			intMod = mod
		case intHandshakeChannel:
			intMod = mod
		case intBufferedChannel:
			intMod = mod
		case intProcModule:
			err, intMod = abstractForDebugFromProcModuleAtoH(mod)
		}
		if err != nil {
			return err, nil
		}
		retIntMods = append(retIntMods, intMod)
	}
	return nil, retIntMods
}

func abstractForDebugFromProcModuleItoK(mod intProcModule) (error, intProcModule) {
	newTrans := []intTransition{}
	midState := intState("mid_state1")
	{
		newTran := intTransition{
			FromState: intState("eps_state76"),
			NextState: midState,
			Condition: "",
			Actions:   []intAssign{},
		}
		newTrans = append(newTrans, newTran)
	}
	midState2 := intState("mid_state2")
	{
		newTran := intTransition{
			FromState: midState,
			NextState: midState2,
			//FromState: midState2,
			//NextState: intState("eps_state79"),
			Condition: "",
			Actions:   []intAssign{},
		}
		newTrans = append(newTrans, newTran)
	}
	for _, tran := range mod.Trans {
		if tran.FromState == "eps_state76" && tran.NextState != "state156" && tran.NextState != "state174" {
			newTran := intTransition{
				FromState: midState2,
				NextState: tran.NextState,
				Condition: tran.Condition,
				Actions:   tran.Actions,
			}
			newTrans = append(newTrans, newTran)
		} else if tran.FromState == "eps_state76" && tran.NextState != "state156" {
			//if tran.FromState == "eps_state76" && tran.NextState != "state156" {
			newTran := intTransition{
				FromState: midState,
				NextState: tran.NextState,
				Condition: tran.Condition,
				Actions:   tran.Actions,
			}
			newTrans = append(newTrans, newTran)
			//} else if tran.NextState == "eps_state79" && tran.FromState != "state168" {
			//	newTran := intTransition{
			//		FromState: tran.FromState,
			//		NextState: midState2,
			//		Condition: tran.Condition,
			//		Actions:   tran.Actions,
			//	}
			//	newTrans = append(newTrans, newTran)
		} else {
			newTrans = append(newTrans, tran)
		}
	}

	retMod := intProcModule{
		Name:      mod.Name,
		Args:      mod.Args,
		Vars:      mod.Vars,
		InitState: mod.InitState,
		Trans:     newTrans,
		Defaults:  mod.Defaults,
		Defs:      mod.Defs,
	}

	return nil, retMod
}

func abstractForDebugFromProcModuleAtoH(mod intProcModule) (error, intProcModule) {
	newTrans := []intTransition{}
	nextStateNum := 0
	for _, tran := range mod.Trans {
		if tran.FromState == "eps_state76" {
			stateToNum := map[intState]int{
				"state156": 0,
				"state174": 1,
				"state190": 2,
				"state206": 3,
				"state222": 4,
				"state238": 5,
				"state254": 6,
				"state270": 7,
			}
			num_eps := stateToNum[tran.NextState]
			states := []intState{tran.FromState}
			for i := 0; i < num_eps; i++ {
				newState := intState(fmt.Sprintf("mid_state%d", nextStateNum))
				nextStateNum++
				states = append(states, newState)
			}
			for i := 0; i < len(states)-1; i++ {
				fromState := states[i]
				nextState := states[i+1]
				newTran := intTransition{
					FromState: fromState,
					NextState: nextState,
					Condition: "",
					Actions:   []intAssign{},
				}
				newTrans = append(newTrans, newTran)
			}
			newTran := intTransition{
				FromState: states[len(states)-1],
				NextState: tran.NextState,
				Condition: tran.Condition,
				Actions:   tran.Actions,
			}
			newTrans = append(newTrans, newTran)
		} else {
			newTrans = append(newTrans, tran)
		}
	}

	retMod := intProcModule{
		Name:      mod.Name,
		Args:      mod.Args,
		Vars:      mod.Vars,
		InitState: mod.InitState,
		Trans:     newTrans,
		Defaults:  mod.Defaults,
		Defs:      mod.Defs,
	}

	return nil, retMod
}

// ==== Abstraction: Replace conditions with true (except channel actions) ====

func replaceConditionsWithTrue(mods []intModule) (error, []intModule) {
	retIntMods := []intModule{}
	for _, mod := range mods {
		var intMod intModule
		var err error
		switch mod := mod.(type) {
		case intMainModule:
			intMod = mod
		case intHandshakeChannel:
			intMod = mod
		case intBufferedChannel:
			intMod = mod
		case intProcModule:
			err, intMod = replaceConditionsWithTrueFromProcModule(mod)
		}
		if err != nil {
			return err, nil
		}
		retIntMods = append(retIntMods, intMod)
	}
	return nil, retIntMods
}

func replaceConditionsWithTrueFromProcModule(mod intProcModule) (error, intProcModule) {
	newTrans := []intTransition{}
	for _, tran := range mod.Trans {
		var newCond string
		if strings.Contains(tran.Condition, ".ready") || strings.Contains(tran.Condition, ".received") {
			newCond = tran.Condition
		} else {
			newCond = ""
		}
		newTran := intTransition{
			FromState: tran.FromState,
			NextState: tran.NextState,
			Condition: newCond,
			Actions:   tran.Actions,
		}
		newTrans = append(newTrans, newTran)
	}

	retMod := intProcModule{
		Name:      mod.Name,
		Args:      mod.Args,
		Vars:      mod.Vars,
		InitState: mod.InitState,
		Trans:     newTrans,
		Defaults:  mod.Defaults,
		Defs:      mod.Defs,
	}

	return nil, retMod
}

// ==== Abstraction: Remove all epsilon-transitions ====

func removeEpsFromIr1(mods []intModule) (error, []intModule) {
	retIntMods := []intModule{}
	for _, mod := range mods {
		var intMod intModule
		var err error
		switch mod := mod.(type) {
		case intMainModule:
			intMod = mod
		case intHandshakeChannel:
			intMod = mod
		case intBufferedChannel:
			intMod = mod
		case intProcModule:
			err, intMod = removeEpsFromProcModule(mod)
		}
		if err != nil {
			return err, nil
		}
		retIntMods = append(retIntMods, intMod)
	}
	return nil, retIntMods
}

func removeEpsFromProcModule(mod intProcModule) (error, intProcModule) {
	newTrans := mod.Trans
	nextStateNum := 0
	for {
		found, epsTran := searchEpsTranWhichCanBeRemoved(newTrans, mod.InitState)
		if !found {
			break
		}
		newState := intState(fmt.Sprintf("eps_state%d", nextStateNum))
		newTrans = constructReducedTrans(newTrans, epsTran.FromState, epsTran.NextState, newState)
		nextStateNum++
	}

	retMod := intProcModule{
		Name:      mod.Name,
		Args:      mod.Args,
		Vars:      mod.Vars,
		InitState: mod.InitState,
		Trans:     newTrans,
		Defaults:  mod.Defaults,
		Defs:      mod.Defs,
	}

	return nil, retMod
}

func collectTransFrom(trans []intTransition, state intState) []intTransition {
	filteredTrans := []intTransition{}
	for _, tran := range trans {
		if tran.FromState == state {
			filteredTrans = append(filteredTrans, tran)
		}
	}
	return filteredTrans
}

func collectTransTo(trans []intTransition, state intState) []intTransition {
	filteredTrans := []intTransition{}
	for _, tran := range trans {
		if tran.NextState == state {
			filteredTrans = append(filteredTrans, tran)
		}
	}
	return filteredTrans
}

func searchEpsTranWhichCanBeRemoved(trans []intTransition, initState intState) (bool, intTransition) {
	for _, tran := range trans {
		if tran.FromState != initState && tran.FromState != tran.NextState && isEps(tran) {
			if !(len(collectTransFrom(trans, tran.FromState)) > 1 && len(collectTransTo(trans, tran.NextState)) > 1) {
				return true, tran
			}
		}
	}
	return false, intTransition{}
}

func isEps(tran intTransition) bool {
	return tran.Condition == "" && len(tran.Actions) == 0
}

func constructReducedTrans(trans []intTransition, p intState, q intState, r intState) []intTransition {
	newTrans := []intTransition{}
	for _, tran := range trans {
		if tran.FromState == p && tran.NextState == q && isEps(tran) {
			continue
		}

		newFromState := tran.FromState
		if newFromState == p || newFromState == q {
			newFromState = r
		}
		newNextState := tran.NextState
		if newNextState == p || newNextState == q {
			newNextState = r
		}

		newTran := intTransition{
			FromState: newFromState,
			NextState: newNextState,
			Condition: tran.Condition,
			Actions:   tran.Actions,
		}
		newTrans = append(newTrans, newTran)
	}

	return newTrans
}
