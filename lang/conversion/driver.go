package conversion

import (
	. "bitbucket.org/sierran/sandal/lang/data"
	"fmt"
	"github.com/k0kubun/pp"
	"log"
	"strings"
)

func ConvertASTToNuSMV(defs []Def, abst bool) (string, error) {
	err, ir1 := astToIr1(defs, abst)
	if err != nil {
		return "", err
	}

	err, ir2s := ir1ToIr2(ir1)
	if err != nil {
		return "", err
	}

	mods := []string{}
	for _, ir2 := range ir2s {
		mods = append(mods, instantiateTemplate(ir2))
	}

	return strings.Join(mods, ""), nil
}

// -- debug functions --

func DumpIR1(defs []Def, abst bool) {
	err, intMods := astToIr1(defs, abst)
	if err != nil {
		log.Fatal("Conversion error: ", err)
	}
	pp.Println(intMods)
}

func DumpIR2(defs []Def, abst bool) {
	err, intMods := astToIr1(defs, abst)
	if err != nil {
		log.Fatal("Conversion error: ", err)
	}

	err, tmplMods := ir1ToIr2(intMods)
	if err != nil {
		log.Fatal("Conversion error: ", err)
	}
	pp.Println(tmplMods)
}

func DumpGraph(defs []Def, abst bool) {
	err, intMods := astToIr1(defs, abst)
	if err != nil {
		log.Fatal("Conversion error: ", err)
	}
	dumpGraphFromIr1(intMods)
}

func dumpGraphFromIr1(intMods []intModule) {
	for i, intMod := range intMods {
		switch intMod.(type) {
		case intProcModule:
			fmt.Printf("digraph MyGraph%d {\n", i)
			mod := intMod.(intProcModule)
			for _, trans := range mod.Trans {
				acts := []string{}
				for _, act := range trans.Actions {
					acts = append(acts, act.LHS+":="+act.RHS)
				}
				cond := trans.Condition
				if len(cond) > 0 {
					fmt.Printf("	%s -> %s [label=\"%s\\n/\\n%s\"]\n", trans.FromState, trans.NextState, trans.Condition, strings.Join(acts, "\\n"))
				} else {
					fmt.Printf("	%s -> %s [label=\"%s\\n/\\n%s\"]\n", trans.FromState, trans.NextState, "TRUE", strings.Join(acts, "\\n"))
				}
			}
			fmt.Printf("}\n")
		}
	}
}
