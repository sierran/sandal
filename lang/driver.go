package lang

import (
	"bitbucket.org/sierran/sandal/lang/conversion"
	"bitbucket.org/sierran/sandal/lang/parsing"
	"bitbucket.org/sierran/sandal/lang/typecheck"
	"github.com/k0kubun/pp"
	"log"
)

func CompileFile(body string, abst bool) (string, error) {
	scanner := new(parsing.Scanner)
	scanner.Init([]rune(body), 0)
	defs := parsing.Parse(scanner)

	if err := typecheck.TypeCheck(defs); err != nil {
		return "", err
	}

	return conversion.ConvertASTToNuSMV(defs, abst)
}

// -- debug functions --

func DumpAST(body string) {
	scanner := new(parsing.Scanner)
	scanner.Init([]rune(body), 0)
	defs := parsing.Parse(scanner)

	pp.Println(defs)
}

func DumpIR1(body string, abst bool) {
	scanner := new(parsing.Scanner)
	scanner.Init([]rune(body), 0)
	defs := parsing.Parse(scanner)

	if err := typecheck.TypeCheck(defs); err != nil {
		log.Fatal("TypeCheck error:", err)
	}

	conversion.DumpIR1(defs, abst)
}

func DumpIR2(body string, abst bool) {
	scanner := new(parsing.Scanner)
	scanner.Init([]rune(body), 0)
	defs := parsing.Parse(scanner)

	if err := typecheck.TypeCheck(defs); err != nil {
		log.Fatal("TypeCheck error:", err)
	}

	conversion.DumpIR2(defs, abst)
}

func DumpGraph(body string, abst bool) {
	scanner := new(parsing.Scanner)
	scanner.Init([]rune(body), 0)
	defs := parsing.Parse(scanner)

	if err := typecheck.TypeCheck(defs); err != nil {
		log.Fatal("TypeCheck error:", err)
	}

	conversion.DumpGraph(defs, abst)
}
